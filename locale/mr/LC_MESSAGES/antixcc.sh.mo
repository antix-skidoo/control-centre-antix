��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3  7   �  "        3  R   K     �  5   �  (   �  7     1   Q  "   �  7   �  8   �  >     2   V  %   �  .   �  ;   �  V     W   q  @   �  �   
  R   �     �  Z   �     U  !   k  =   �  B   �  5     ?   D  Z   �  >   �  <     M   [  7   �     �  (   �  H   &  K   o  	   �  /   �  /   �  H   %     n  A   �     �  ^   �  .   B     q  +   �  2   �  S   �     :  (   Z  ;   �  C   �  C     +   G  G   s  +   �     �  0   �     ,  3   9  7   m  0   �  S   �  B   *  Q   m  L   �  H        U  ;   h  J   �     �  "        (  A   H  :   �  0   �  /   �  M   &          *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:26+0300
Last-Translator: Prachi Joshi <josprachi@yahoo.com>
Language-Team: Marathi (http://www.transifex.com/anticapitalista/antix-development/language/mr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mr
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
  डिस्कॉननेक्टशेअर्स वैयक्तिक मदत 1 ते 1 आवाज एडीएसएल / पीपीपीओई कॉन्फिगरेशन अ‍ॅडब्लॉक मिक्सर समायोजित करा Alsamixer इक्वेलाइझर विकल्प कॉन्फिग्रेटर बॅकलाइट ब्राइटनेस बूट दुरुस्ती ब्रॉडकॉम व्यवस्थापक सत्रासाठी कीमॅप बदला स्लिम पार्श्वभूमी बदला प्रारंभ सेवा निवडा वॉलपेपर निवडा कोडेक्स इंस्टॉलर ऑटोमाउंट कॉन्फिगर करा जीपीआरएस / यूएमटीएस कॉन्फिगर करा लाइव्ह पर्सिस्टन्स कॉन्फिगर करा कनेक्टशेअर कॉन्फिगरेशन लाइव्ह-यूएसबी रीटेन विभाजने तयार करा (युनेटबूटिन) स्वरूप आणि अनुभव सानुकूलित करा डेस्कटॉप डायल-अप कॉन्फिगरेशन (जीनोम पीपीपी) डिस्क्स ड्रायव्हर्स ड्रोपी (फाईल सामायिकरण) बूटलोडर मेनू संपादित करा फाइल्स कॉन्फिगर करा फायली वगळणे संपादित करा फ्लक्सबॉक्स सेटिंग्ज संपादित करा IceWM सेटिंग्ज संपादित करा JWM सेटिंग्ज संपादित करा सिस्टम मॉनिटर (Conky) संपादित करा फायरवॉल कॉन्फिगरेशन हार्डवेअर आयएसओ स्नॅपशॉट एका विभाजनाची प्रतिमा बनवा अँटीएक्स लिनक्स स्थापित करा थेट थेट यूएसबी मेकर (cli) थेट यूएसबी मेकर (gui) लाइव्ह-यूएसबी कर्नल अपडेटर देखभाल पॅकेजेस व्यवस्थापित करा मेनू संपादक कनेक्ट केलेले डिव्हाइसेस माउंट करा माउस कॉन्फिगरेशन नेटवर्क नेटवर्क सहाय्यक नेटवर्क इंटरफेस (Ceni) एनव्हीडिया ड्राइव्हर इंस्टॉलर पीसी माहिती पॅकेज इंस्टॉलर ड्राइव्हचे विभाजन करा संकेतशब्द सूचना (सु / सुडो) प्राधान्यीकृत अनुप्रयोग मुद्रण सेटिंग्ज रीमास्टर-सानुकूलित लाइव्ह रेपो व्यवस्थापक SSH मार्ग रूट चिकाटी जतन करा सत्र स्वयं लॉगिन सेट करा तारीख आणि वेळ सेट करा फॉन्ट आकार सेट करा ग्रब बूट प्रतिमा सेट करा (केवळ png) स्क्रीन ब्लँकिंग सेट करा स्क्रीन रिझोल्यूशन (ARandR) सेट करा सिस्टम कीबोर्ड लेआउट सेट करा लाइव्ह पर्सिस्टन्स सेट करा शेअर्स ध्वनी कार्ड निवडकर्ता निर्देशिका सिंक्रोनाइझ करा सिस्टिम सिस्टम बॅकअप चाचणी ध्वनी वापरकर्ता डेस्कटॉप-सत्र वापरकर्ता व्यवस्थापक WPA Supplicant कॉन्फिगरेशन वायफाय कनेक्ट (Connman) विंडोज वायरलेस ड्राइव्हर्स् 