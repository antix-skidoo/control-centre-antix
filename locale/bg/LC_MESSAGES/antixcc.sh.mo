��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �       *   )     T  !   \     ~  4   �  3   �  *   �     &  :   @  !   {  9   �     �  (   �  )      &   J  2   q  +   �  ]   �  E   .     t  3   �     �     �  /   �  /     F   7  3   ~  -   �  1   �  *     ;   =  =   y     �     �  !   �  '   �       1   &  1   X  )   �     �  %   �     �  :     ,   B  
   o     z  *   �  1   �     �  &     %   4  .   Z  1   �  "   �  D   �  (   #     L  0   X  
   �  *   �  %   �  4   �  G     3   b  9   �  D   �  2        H  )   U  3        �  ,   �     �  4     ,   =  .   j  0   �  ,   �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:22+0300
Last-Translator: Peyu Yovev (Пею Йовев) <spacy00001@gmail.com>
Language-Team: Bulgarian (http://www.transifex.com/anticapitalista/antix-development/language/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
  DisconnectShares 1-към-1 Помощ 1-към-1 Глас Конфигуриране на ADSL/PPPOE Adblock Настройте Миксера Alsamixer Equalizer Конфигуратор на алтернативи Яркост на фоновата светлина Поправка на стартиране Broadcom мениджър Промяна на клавишите за сесията Сменете Фона на Slim Изберете Услуги При Стартиране Изберете Картина Инсталиране на кодеци Конфигуриране на automount Конфигурирайте GPRS/UMTS Настройте лайф постоянство Конфигурация на Connectshares Създаване на Live-USB със запазване на дяловете (UNetbootin) Персонализирайте изгледа и усещането Десктоп Конфигуриране на Dial-Up (GNOME PPP) Дискове Драйвери Droopy (Споделяне на файлове) Редактиране на Bootloader меню Редактирайте Конфигурационни Файлове редактиране на файлове Exclude  Настройте Fluxbox Настройки Редактирайте IceWM Настройки Промяна на JWM настройки Промяна на системен монитор (Conky) Конфигуриране на защитната стена Хардуер ISO Snapshot Изобразете Сектор Инсталиране на antiX Linux Лайф Създател на жив USB образ (cli) Създател на жив USB образ (gui) Обновяване на Live-USB Kernel Поддръжка Управлявайте Пакети Меню редактор Монтирайте Свързани Устройства Конфигурация на мишката Мрежа Мрежов асистент Мрежови Интерфейси (Ceni) Инсталатор на Nvidia драйвери PC Информация Инсталатор на пакети Поделете Устройство Поискване за парола (su/sudo) Предпочтительные Програми Настройки за печат Промяна-Персонализиране на жив образ Мениджър на хранилища SSH Conduit Съхранете рут постоянство Сесия Конфигуриране на Auto-Login Настройте Дата и Час Задаване на размер на шрифта Задаване на Grub стартиращ образ (само png) Задаване изгасяне на екрана Задайте Екранна Резолюция (ARandR) Задаване на подредба на клавиатурата Настройте лайф постоянство Дялове Избор на звукова карта Синхронизирайте Директории Система Системно резервно копие Тест на звука Потребителска десктоп-сесия Мениджър на потребители Конфигуриране на WPA Supplicant Свържете Се Безжично (Connman) MS Windows безжични драйвери 