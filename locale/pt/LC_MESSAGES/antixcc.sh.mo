��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �  	   �     	          =     I     `     x     �     �     �  ,   �     �          /     D     \     {     �  :   �     �        "        1     C     Q  '   p  "   �     �  !   �     �       #   6  #   Z     ~     �     �     �     �     �     �  %   	     /     <     U     c     �     �     �     �     �     �  !        #  %   =      c     �  '   �     �     �     �               .     :     K     g     |     �     �  	   �     �     �            
   !      ,     M      `     �  9   �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-27 20:30+0200
Last-Translator: José Vieira <jvieira33@sapo.pt>
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Desligar Partilhas Assistência 1-a-1 Voz 1-a-1 Ligação ADSL/PPPOE Bloquear publicidade (Adblock) Ajustar som Equalizador Alsamixer  Configurar Alternativas Brilho do Ecrã Reparar o Arranque Gestor Broadcom Esquema de teclado da sessão Alterar fundo do SLiM [Simple Login Manager] Serviços a iniciar no arranque Escolher Imagem de Fundo Instalador de Codecs Configurar Automontagem Configurar ligação GPRS/UMTS Configurar Persistência Partilhas - Configurar Criar USB-executável mantendo as partições (UNetbootin) Personalizar a aparência Ambiente Trab Ligação por telefone (GNOME PPP) Gestão de Discos Controladores Droopy (Partilha de ficheiros) Editar o menu do Carregador do Arranque Editar ficheiros de configuração Editar 'Excluir ficheiros' Editar as definições do Fluxbox Editar as definições do IceWM Editar as definições do JWM Editar o Conky (monitor do sistema) Configuração da Barreira/Firewall Equipamento Capturas ISO Imagens de partições Instalar o antiX Sistema externo Criar USB-Executável (L.C.) Criar USB-Executável (I.G.) Actualizar núcleo da USB-executável Manutenção Gerir Pacotes (Synaptic) Editar o Menu Montar Dispositivos Acoplados Configuração do Rato Rede Assistente de Rede Interfaces de rede (Ceni) Instalar Controlador Nvidia Informação do computador Instalar/desinstalar Aplicações Criar e gerir partições Opções de administração (su/sudo) Pré-definir aplicações a usar Definições de impressão Recompor/Personalizar a USB-executável Gestor de Repositórios Ligação segura SSH Guardar persistência de root Sessão Entrada automática Data e Hora Tamanho da fonte Imagem do GRUB (apenas png) Protecção do Ecrã Resolução do Ecrã (ARandR) Teclado do sistema Estabelecer Persistência Partilhas Seleccionar Placa de Som Sincronizar Directórios Sistema Cópias de segurança Testar som Configurar Sessão de Utilizador Gerir utilizadores Configuração do WPA Supplicant Ligação WiFi (Connman) Controladores Windows para Ligações Sem Fios (wireless) 