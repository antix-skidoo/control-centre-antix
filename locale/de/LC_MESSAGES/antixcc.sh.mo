��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �                '     /     @  #   `  !   �     �  #   �  )   �          ,     D     X     i     �     �     �  <   �          *  &   6     ]     l     t  "   �     �     �     �     �     
  !        >     U     ^     p     �     �     �     �     �     �     �          #     >     Q     Z     m     �     �     �     �  !   �               2     J     W  $   f     �     �     �     �     �     �       &   $     K  	   f     p     �     �     �     �  "   �     �     �          +         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:24+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: German (http://www.transifex.com/anticapitalista/antix-development/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Freigaben trennen 1-zu-1_Hilfe 1-zu-1 Stimme ADSL/PPPOE konfigurieren Adblock Toneinstellungen Alsamixer Frequenzgangkorrektur Alternativanwendungen konfigurieren Helligkeit Hintergrundbeleuchtung Bootreparatur (Urladeprozedur) Manager für Broadcom WiFi-Treiber  Tastaturbelegung für die Sitzung ändern Slim-Hintergrund ändern Startup-Dienste wählen Hintergrund wählen Codecs Installer Automount konfigurieren GPRS-UMTS-Konfiguration Dauerhaftigkeit konfigurieren ConnectShares Konfiguration Mit dauerhaften Partitionen auf USB installieren (UNetbooin) Erscheinungsbild anpassen Oberfläche Wählleitung konfigurieren (Gnome PPP) Speichermedien Treiber Droopy (File Sharing) Menü des Bootprogramms bearbeiten Konfigurationsdateien Ausschlussdateien bearbeiten Fluxbox Einstellungen IceWM Einstellungen JWM Einstellungen System-Monitor (Conky) einstellen Firewall Konfiguration Hardware ISO-Schnappschuss Abzug einer Partition erstellen antiX Linux installieren Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernelaktualisierung Wartung Verwalten der Pakete Menüverwaltung Geräte einhängen (mount) Maus-Konfiguration Netzwerk Netzwerk-Assistent Netzwerk-Assistent (Ceni) Nvidia-Treiber Installation PC Information Paketinstallation Partitionieren eines Laufwerks Passwortabfrage (su/sudo) wählen Bevorzugte Anwendungen Drucker-Einstellungen Live anpassen-remastern Repo Manager SSH-Einbindung Dauerhaftigkeit für root speichern  Sitzung Auto-login einstellen Datum & Zeit Schriftgröße setzen Hintergrund von GRUB (png) Bildschirmlöschung einstellen Bildschirmauflösung (ARandR) Systemweite Tastaturbelegung festlegen Dauerhaftigkeit einstellen Freigaben Soundkartenwahl Verzeichnissynchronisation System System sichern Klang testen Konfiguration der Benutzersitzung  Benutzerverwaltung WPA Supplicant Konfiguration WiFi Verbindung (Connman) Windows WLAN-Treiber 