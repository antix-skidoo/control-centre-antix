��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �  	             5     =     Q     h     �     �     �  '   �     �     �          4     M     a  '   u     �  -   �  %   �  
     %        A     G      T     u     �     �     �     �       %   "     H  	   c     m     ~     �     �     �     �  !   �     �          "     4     Q     k     q     �  #   �     �     �     �  "        *     A  "   X     {     �     �     �     �     �  %   �  $     !   A  +   c  +   �     �     �     �                     4     B     a     s     �  "   �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:23+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Catalan (http://www.transifex.com/anticapitalista/antix-development/language/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Desconnecta comparticions Assistència_1-a-1 Veu_1-a-1 Configuració de ADSL/PPPoE Adblock Ajusta el Mesclador Equalitzador AlsaMixer Configurador d'alternatives Lluminositat del fons Repara l'arrencada  Gestor de Broadcom  Canvia el mapa de teclat per la sessió Canvia Fons de Slim Tria els Serveis d'arrencada Tria el Fons de Pantalla Instal·lador de còdecs Configura Automount Configura GPRS/UMTS Configura la persistència en autònom  Configuració de ConnectShares Crea particions per USB autònom (UNetbootin) Personalitza l'aspecte i comportament Escriptori Configuració de marcatge (GNOME PPP) Discs Controladors Droopy (Compartició de fitxers) Edita el menú d'arrencada  Edita Fitxers de Configuració Edita els fitxers exclosos  Edita Configuració de Fluxbox Edita Configuració d'IceWM Edita Configuració de JWM Edita el Monitor del Sistema (Conky)  Configuració de tallafocs Maquinari Instantània ISO Crea Imatge de Partició Instal·la antiX Linux Live Live USB Maker (CLI) Live USB Maker (IGU) Actualitzador del kernel Live-USB Manteniment Gestiona el Programari Editor del menú  Munta Dispositius Connectats Configuració del ratolí Xarxa Assistent de Xarxa Interfície de xarxa (Ceni) Instal·lador de controlador Nvidia Informació del Sistema Instal•lador de paquets  Parteix un Disc Indicador de contrasenya (su/sudo) Aplicacions preferides paràmtes d'impressió Remasteritza-Personalitza Autònom Gestor de dipòsits  SSH Conduit Desa la persistència de root  Sessió  Activa l'entrada automàtica Defineix Data i Hora Estableix la mida del tipus de lletra Imatge d'arrencada GRUB (només png) Defineix el bloqueig de pantalla  Defineix la resolució de pantalla (ARandR) Canvia la disposició de teclat del sistema Defineix la persistència live Comparticions Selector da Tarja de So Sincronitza Directoris Sistema Còpia de seguretat  Prova del so  Sessió d'Escriptori d'usuari  Gestor d'usuaris  Configuració de WPA Supplicant Connexió WiFi (Connman) Controladors sense fils de Windows 