��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �       	        &     @     W     c     y     �     �     �  '   �     �     
     )     A      V     w     �     �  9   �     �  
     #        A     H  !   V  &   x  "   �     �  $   �  "        )  #   G     k     �     �     �     �     �     �     �  $        3     @     S     b     �     �     �     �     �     �            %   5     [     r  &   �     �     �     �     �  (        -     D     b  $   z  -   �     �     �               /     G     O     c     p     �      �     �  .   �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-27 20:28+0200
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>
Language-Team: Galician (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Desconectar intercambios Asistencia 1-a-1 Voz 1-a-1 Configuración ADSL/PPPOE Bloqueador de anuncios Axustar son Ecualizador Alsamixer Configurar alternativas Brillo da pantalla Reparador do arranque Xestor broadcom Cambiar o esquema do teclado da sesión Cambiar o fondo do SLiM Escoller o servizo de arranque Escoller imaxe de fondo Instalador de Codecs Configurar conexión automática Configurar GPRS/UMTS Configurar persistencia Intercambios - Configurar Crear USB-executable mantendo as particións (UNetbootin) Personalizar a apariencia Escritorio Configurar no teléfono (GNOME PPP) Discos Controladores Droopy (Intercambio de ficheiros) Editar o menú do cargador do arranque Editar ficheiros da configuración Editar os ficheiros excluídos Editar as configuracións de Fluxbox Editar as configuracións de IceWM Editar a configuracion de JWM Editar o monitor do sistema (Conky) Configuración do cortalumes Equipamento Capturas ISO Imaxes de particións Instalar o antiX Sistema externo Crear USB-executable (cli) Crear USB-executable (gui) Actualizar núcleo do USB-executable Manutención Xestionar paquetes Editar o menú Conectar dispositivos acoplados Configuración do rato Rede Asistente da rede interfaces da rede (Ceni) Instalar controlador Nvidia Información do computador Instalador do paquete Crear e xestionar particións Opcións da administración (su/sudo) Aplicativos preferidas Configuración da impresora Recompor-Personalizar o USB-executable Xestor de repositorios Ligazón segura SSH Gardar persistencia do root Sesión Establecer inicio de sesión automático Establecer Data e Hora Establecer o tamaño da fonte Imaxe do GRUB (só png) Establecer a protección da pantalla Establecer a resolución da pantalla (ARandR) Establecer o teclado do sistema Establecer persistencia Intercambios Seleccionar a tarxeta do son Sincronizar directorios Sistema Copias de seguranza Probar o son Configurar sesión de usuario Xestionar usuarios Configuración do WPA Supplicant Conexión WiFi (Connman) Controladores Windows para ligazóns sen fíos 