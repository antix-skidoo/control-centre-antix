��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �               ,     4     C     W     o     �     �  )   �     �     �               (     ?     Q     n  6   �     �  	   �      �     
          "     6     P     m     �     �     �  #   �       	   !     +     8     M     d     i     ~     �     �     �     �     �     �     �               7     U     d     w     �     �     �     �     �               #     +     F     ^  &   q     �  $   �     �     �               )     @     G  
   U     `     ~     �     �     �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:27+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Swedish (http://www.transifex.com/anticapitalista/antix-development/language/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
  DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Konfiguration Adblock Justera Mixern Alsamixer Equalizer Alternativ-konfigurator Bakgrundsljusets Styrka  Boot-reparation Broadcom-hanterare Ändra Tangentbordslayout för Sessionen  Byt Slim bakgrund Välj Programstart-tjänster Välj Wallpaper Codecs Installerare Konfigurera Automount  Anpassa GPRS/UMTS Konfigurera Live Persistens  Connectshares Konfiguration  Skapa Live-USB med Behållna Partitioner (UNetbootin)  Anpassa Utseende och Känsla Skrivbord Dial-Up Inställning (GNOME PPP) Hårddiskar Drivrutiner Droopy (Fildelning) Redigera Bootloader-meny  Redigera Konfigurationsfiler Redigera Exkludera Filer Redigera Fluxbox inställningar Redigera IceWM's inställningar Redigera JWM Inställningar Redigera Systemövervakare (Conky)  Inställning av Brandvägg Hårdvara ISO Snapshot Avbilda en Partition Installera antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kärna-Uppdaterare  Skötsel Hantera Paket Meny-editor Montera Anslutna Enheter Mus-inställning Nätverk Nätverks-assistent  Nätverksgränssnitt (Ceni) Nvidia drivrutin-installerare PC Information Paket-installerare Partitionera en Hårddisk Lösenordsfönster (su/sudo) Föredragna Applikationer Skrivarinställningar Remaster-Anpassa Live Repo Manager SSH Conduit Spara Root Persistens Session Ställ in Auto-inloggning  Ställ in Datum och Tid Välj Font-storlek Ställ in Grub Boot Image (endast png) Ställ in Skärm Tömning Ställ in Skärmupplösning (ARandR) Ställ in Tangentbordslayout  Ställ in Live Persistens Andelar Ljudkortsväljare Synkronisera Kataloger System System Backup Testa Ljud Användare Skrivbords-Session Användarhanterare WPA Supplicant Inställning Ansluta WiFi  (Connman) Windows Trådlösa Drivrutiner 