��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3     �     �  
             .     6     N     g     }     �     �  "   �     �     �                &     A      U     v  3   �     �     �     �     �     �          !  !   :  !   \      ~     �     �     �     �     	               >     S     X     l     �     �     �     �     �     �     �     �  "        4     K     [     n     �     �     �  !   �     �  
   �               "     5     H  !   X     z     �     �     �     �     �     	     "     *  
   =     H     `     p     �     �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:26+0300
Last-Translator: Pierluigi Mario <pierluigimariomail@gmail.com>
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Disconnetti Condivisioni Assistenza 1 a 1 Voce 1 a 1 Configura ADSL/PPPOE Adblock Alsa-Mixer Canali Audio Alsa-Mixer Equalizzatore Configura Alternative Retroilluminazione Schermo Ripara Boot Broadcom Manager Mappatura Tastiera per la Sessione Sfondo di Login (Slim) Servizi di Avvio Cambia Sfondo Desktop Installa Codecs Configura l'auto-montaggio Configura GPRS/UMTS Configura Persistenza della Live Imposta Condivisioni Crea Live-USB mantenendo le partizioni (UNetbootin) Aspetto e Stile Desktop Configura Dial-Up (Gnome PPP) Dischi Driver Droopy (Condivisioni di file) Modifica il menu di boot Modifica i file di configurazione Modifica il file delle esclusioni Modifica impostazioni di Fluxbox Modifica impostazioni di IceWM Modifica impostazioni di JWM Monitor di Sistema (Conky) Configura Firewall Hardware Snapshot ISO Crea Immagine della Partizione Installa antiX Linux Live Crea USB-Live (cli) Crea USB-Live (gui) Aggiorna Kernel della Live-USB Manutenzione Gestore Pacchetti Menu Editor Monta Dispositivi Configura il Mouse Rete Risoluzione Problemi di Rete Gestisci Interfacce di Rete (ceni) Installa driver Nvidia Informazioni PC Installa Programmi Partiziona un Disco Richiesta Password (su/sudo) Applicazioni Preferite Imposta Stampante Rimasterizza-Personalizza la Live Repo Manager Tunnel SSH Salva Persistenza Root Sessione Imposta Auto-Login Imposta Data e Ora Dimensioni Font Immagine del boot Grub (solo png) Sospensione Schermo Risoluzione Schermo (ARandR) Mappatura Tastiera di Sistema Imposta Persistenza della Live Condivisioni Scegli Scheda Audio Sincronizza le Directory Sistema Backup del sistema Audio Test Sessione Desktop Utente Gestione Utenti Configura WPA Supplicant Connetti WiFi (Connman) Driver wireless di Windows 