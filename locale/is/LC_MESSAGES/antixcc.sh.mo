��    J      l  e   �      P     Q     c     u     �     �     �     �     �     �     �     �          *     A     Y     j     {     �     �  .   �     �                         0     B     U     k          �     �     �     �     �     �     �     	     	     "	     .	     F	     Z	     b	     |	     �	     �	     �	     �	     �	     �	     
     
     +
     A
     I
     X
     j
     x
     �
     �
     �
     �
     �
               /     6  
   D     O     d     q     �  �  �     h     {     �     �     �     �     �     �               +  %   ?     e     {     �     �  ,   �     �  0     @   8     y  
   �     �     �     �     �     �     �          (     >     ]     k     {     �     �     �     �     �     �     �               (     ;     T     k     �     �     �     �     �     �  /   �     '     ,     L     h  (   ~     �     �  $   �  3   �  	   3     =     Q     c     i     ~     �     �  $   �  (   �     .                   /   7          5              A   0          <      ,   :   2   #   >   "   '             1             $   6   ;   4   8       -               9      )          I      F                *   !   B          3       @   	   D      =          +   (                  
                          &              C      E       ?   G      J   H             %     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Disks Drivers Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:26+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Icelandic (http://www.transifex.com/anticapitalista/antix-development/language/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
X-Generator: Poedit 2.3
 Aftengja sameignir 1-á-1 aðstoð 1-á-1 samtal ADSL/PPPOE uppsetning Adblock Aðlaga hljóðblöndun Alsamixer tónjafnari Stilling annarra möguleika Styrkur baklýsingar Gera við ræsiferli Sýsla með netkort Breyta lyklaborðsuppsetningu í setu Breyta Slim-bakgrunni Veldu þjónustur við ræsingu Velja bakgrunnsmynd Uppsetning kóðunarlykla Stilla sjálfvirkar tengingar í skráakerfi Stilla GPRS/UMTS Stilla varanlega gagnageymslu í keyrsluumhverfi Búa til Live-USB keyrsludisk og halda disksneiðum (UNetbootin) Sérsníða útlit og hegðun Skjáborð Diskar Reklar Breyta ræsivalmynd Sýsla með stillingaskrár Breyta Exclude-skrám Breyta stillingum Fluxbox Breyta stillingum IceWM Breyta stillingum JWM Sýsla með kerfisvakt (Conky) Vélbúnaður ISO-skyndiafrit Gera diskmynd Setja upp antiX Linux Live-keyrslukerfi Live-usb uppfærsla kjarna Viðhald Sýsla með pakka Valmyndaritill Tengja tæki við skrárkeri Stilla mús Netkerfi Netviðmót (Ceni) Uppsetning Nvidia-rekils Upplýsingar um tölvu Pakkauppsetningarforrit Sneiða diskadrif Kvaðning lykilorðs (su/sudo) Sjálfgefin forrit Prentun Remaster-Sérsníða Live SSH-tengibrú Vista varanlega gagnageymslu kerfisstjóra/root Seta Setja sjálfvirka innskráningu Stilla dagsetningu og tíma Stilla stærð leturs Bakgrunnur Grub ræsingar (einungis PNG) Stilla skjálæsingu Stilla skjáupplausn (ARandR) Stilla lyklaborðsuppsetningu kerfis Setja upp varanlega gagnageymslu í keyrsluumhverfi Sameignir Val á hljóðkorti Samstilla möppur Kerfi Öryggisafrit kerfis Prófa hljóð Skjáborðsseta notanda Sýsla með notendur Tengjast þráðlausu neti (Connman) MS Windows reklar fyrir þráðlaust net 