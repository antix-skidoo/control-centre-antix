��    J      l  e   �      P     Q     c     u     �     �     �     �     �     �     �     �          *     A     Y     j     {     �     �     �     �     �  "   �          #     +     A     V     h     ~     �     �     �     �     �     �     	     	     	     0	     H	     T	     d	     p	     �	     �	     �	     �	     �	     �	     �	     
     (
     7
     D
     P
     f
     n
     }
     �
     �
     �
     �
     �
     
     "     )     <  
   C     N     c     p     �  �  �     A     R     d      q     �     �  .   �  "   �  3        J  $   V  A   {  2   �      �       %   +  7   Q     �  &   �  #   �  3   �     (  2   D     w     �  !   �  &   �     �  5     3   <  1   p  &   �  '   �     �       !   *     L     Q     p  3   �  '   �  '   �       ?   /  -   o     �  C   �  =   �     ;  *   Q  :   |  9   �  -   �           @  #   L     p  E   �  3   �  *   �  K   (  9   t  Q   �  D      &   E     l     s     �     �  ?   �  -   �  $   (  *   M     1                  2   9          (   I      '   C   3      7   >      /   <   4       @       *                            %   8   =   6   :       0               ;      ,          J   $   F      !         -   #   D          5       B   	   E      ?          .   +                  
                         )                      "       A   G         H             &     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Password Prompt (su/sudo) Preferred Applications Print Settings Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser System Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-08 13:39+0300
Last-Translator: VI AhrivenX <pongpeera072@gmail.com>
Language-Team: Thai (http://www.transifex.com/anticapitalista/antix-development/language/th/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.3
 DisconnectShares 1-to-1 Assistance 1-to-1 Voice ตั้งค่า ADSL/PPPOE บล็อคโฆษณา ปรับ Mixer อีควอไลเซอร์ Alsamixer ตั้งค่า Alternatives ความสว่างแบ็คไลท์ Boot Repair ตัวจัดการ Broadcom เปลี่ยน Keymap สำหรับเซสชัน เปลี่ยนพื้นหลัง Slim เลือก Startup Services เลือก Wallpaper ตัวติดตั้ง Codecs ตั้งค่า Mount ฮัตโนมัติ ตั้งค่า GPRS/UMTS ตั้งค่า Live Persistence ตั้งค่า ConnectShares ปรับแต่งรูปลักษณ์ เดสก์ท็อป การตั้งค่า Dial-Up (GNOME PPP) ดิสก์ ไดรเวอร์ Droopy (แชร์ไฟล์) แก้ไขเมนู Bootloader แก้ไข Config Files แก้ไขการตั้งค่า Fluxbox แก้ไขการตั้งค่า IceWM แก้ไขการตั้งค่า JWM แก้ไข System Monitor (Conky) การตั้งค่า Firewall ฮาร์ดแวร์ สร้าง ISO Snapshot ติดตั้ง antiX Linux Live สร้าง Live USB (cli) สร้าง Live USB (gui) อัปเดตเคอร์เนล Live-USB การบำรุงรักษา จัดการแพ็กเกจ แก้ไขเมนู Mount อุปกรณ์ที่เชื่อมต่อ การตั้งค่าเมาส์ เครือข่าย อินเทอร์เฟซเครือข่าย (Ceni) ตัวติดตั้งไดรเวอร์ Nvidia ข้อมูล PC ติดตั้งแพ็กเกจ หน้าต่างรหัสผ่าน (su/sudo) แอปพลิเคชันเริ่มต้น ตั้งค่าการพิมพ์ ตัวจัดการ Repo SSH Conduit บันทึก Root Persistence เซสชัน ตั้งค่าล็อกอินอัตโนมัติ ตั้งเวลาและวันที่ เลือกขนาดฟอนต์ ตั้งค่ารูปภาพ Grub (PNG เท่านั้น) ตั้งค่าการพักหน้าจอ ตั้งค่าความละเอียดหน้าจอ (ARandR) ตั้งค่า Layout คีย์บอร์ดระบบ เปิดใช้ Live persistence Shares เลือก Sound card ระบบ ทดสอบเสียง เซสชันเดสก์ท็อปผู้ใช้ ตัวจัดการผู้ใช้ ตั้งค่า WPA Supplicant เชื่อมต่อ WiFi (Connman) 