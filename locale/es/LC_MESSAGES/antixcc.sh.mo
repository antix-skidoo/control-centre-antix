��    S      �  q   L                #     5     B     [     c     p     �     �     �     �     �     �               *     ;     O     c     ~  .   �     �     �  "   �     	     	     	     0	     E	     W	     j	     �	     �	     �	     �	     �	     �	     �	     
     
     
     /
     D
     \
     h
     x
     �
     �
     �
     �
     �
     �
     �
               /     I     `     o     �     �     �     �     �     �     �     �                ?     Z     r     y     �     �     �  
   �     �     �     �            �  3      �       	             8     @     N     g     �     �     �     �     �     �          .     ?     W  "   l     �  2   �     �  
   �  0        8     ?      M     n  !   �     �     �     �     �  %        3     R     b     o     �  	   �     �     �     �     �               )     H     a     e     x     �     �     �     �     �          *     @     _     l     y     �  !   �     �     �  %   �       (   6  0   _  $   �     �  %   �     �                 !   *     L  !   f     �      �         *         H          K   -       9       
      '       D           E       ?       0   !                 L   F   #   C   5      A   M   G          .          3      )       S          N   1   +          ;           7          @   "   2   >   Q             =   6   4       I           (         R       8   <      	         P      ,                  B   /   $          %             J                    &   O   :     DisconnectShares 1-to-1 Assistance 1-to-1 Voice ADSL/PPPOE Configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight Brightness Boot Repair Broadcom Manager Change Keymap for Session Change Slim Background Choose Startup Services Choose Wallpaper Codecs Installer Configure Automount Configure GPRS/UMTS Configure Live Persistence ConnectShares Configuration Create Live-USB Retain Partitions (UNetbootin) Customize Look and Feel Desktop Dial-Up Configuaration (GNOME PPP) Disks Drivers Droopy (File Sharing) Edit Bootloader Menu Edit Config Files Edit Exclude Files Edit Fluxbox Settings Edit IceWM Settings Edit JWM Settings Edit System Monitor (Conky) Firewall Configuration Hardware ISO Snapshot Image a Partition Install antiX Linux Live Live USB Maker (cli) Live USB Maker (gui) Live-USB Kernel Updater Maintenance Manage Packages Menu Editor Mount Connected Devices Mouse Configuration Network Network Assistant Network Interfaces (Ceni) Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt (su/sudo) Preferred Applications Print Settings Remaster-Customize Live Repo Manager SSH Conduit Save Root Persistence Session Set Auto-Login Set Date and Time Set Font Size Set Grub Boot Image (png only) Set Screen Blanking Set Screen Resolution (ARandR) Set System Keyboard Layout Set Up Live Persistence Shares Sound Card Chooser Synchronize Directories System System Backup Test Sound User Desktop-Session User Manager WPA Supplicant Configuration WiFi Connect (Connman) Windows Wireless Drivers Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 16:25+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Spanish (http://www.transifex.com/anticapitalista/antix-development/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Desconectar recursos compartidos Asistencia_1-a-1 voz_1-a-1 configuración ADSL/PPPOE Adblock Ajuste Sonido Equalizador de Alsamixer Configurador de Alternativos Brillo de luz de fondo Reparar Arranque Administrador de Broadcom Cambiar Keymap para la Sesión Cambiar el fondo de Slim Escoger Servicios para Inicio Fondo de pantalla Instalar códecs Configurar Auto-montaje Configurar GPRS/UMTS Configurar la persistencia en live Configuración de ConnectShares Crear Live-USB y mantener particiones (UNetbootin) Personaliza la Apariencia Escritorio Configuración de Conexión Dial-Up  (GNOME PPP) Discos Controladores Droppy (Intercambio de Archivos) Editar el menú de inicio Editar Archivos de Configuración Editar archivos a excluir Configuración Fluxbox Editar los ajustes de IceWM Editar ajustes de JWM Editar el monitor del sistema (Conky) Configuración del Cortafuegos Soporte físico ISO Snapshot Imagen de Partición Instalar antiX Linux Modo Live Creador de Live USB (cli) Creador de Live USB (gui) Actualiza kernel del LiveUSB Mantenimientos Manejar los paquetes Editor del menú Montar Dispositivos Conectados Configuración del Mouse Red Asistente de Redes Interfaces de Red (Ceni) Instalador controlador Nvidia Información del PC Instalador de Paquetes Particionar Dispositivo Prompt de contraseña (su/sudo) Aplicaciones preferidas Ajustes de Impresión Remasterizar-personalizar vivo Repo Manager SSH Conducto Guardar la persistencia de root Sesión Seleccionar el Inicio Automático Fijar Fecha y Hora Seleccione tamaño de letra Imagen de arranque de Grub (solo png) Ajustar la pantalla en blanco Ajustar Resolución de Pantalla (ARandR) Establecer la Dispoción del Teclado del Sistema Ajustar la persistencia en modo live Recursos compartidos Seleccionador de la Tarjeta de Sonido Sincronizar Directorios Sistema Respaldo Probar el sonido Sesión de escritorio del usuario Administrador de usuarios Configuración del Suplicante WPA Conexión WiFi (Connman) Drivers Inalámbricos de Windows 